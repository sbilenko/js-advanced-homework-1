/*
1. Прототипне наслідування дозволяє об'єктам успадковувати властивості та методи з іншого об'єкта. При створенні об'єкту, він успадковує властивості та методи з його прототипу. Якщо властивість або метод не знайдено на самому об'єкті, JavaScript шукає їх у ланцюжку прототипів, поки не знайде або не досягне кінця ланцюжка (який за замовчуванням є Object.prototype).

2. Ми робимо це тоді, коли хочемо додати до класу-нащадка властивість або метод, якого не буде в батьківському класі. Але для цього ми маємо викликати спочатку батьківський конструктор, а вже потім конструктор нащадка), інакше буде помилка.
*/

/* Створення класу Employee */
class Employee {
    constructor(options) {
        this._name = options.name
        this._age = options.age
        this._salary = options.salary
    }

    get name() {
        return this._name
    }

    set name(newName) {
        this._name = newName
    }

    get age() {
        return this._age
    }

    set age(newAge) {
        this._age = newAge
    }

    get salary() {
        return this._salary
    }

    set salary(newSalary) {
        this._salary = newSalary
    }
}

/* Створення об'єкту на основі класу Employee */
const hr = new Employee({
    name: 'Alice',
    age: 24,
    salary: 1500,
})

/* Створення класу-нащадка */
class Programmer extends Employee {
    constructor(options) {
        super(options)
        this._lang = options.lang
    }

    get salary() {
        return this._salary *= 3
    }

    set salary(newSalary) {
        this._salary = newSalary
    }
}

/* Створення об'єкту на основі класу-нащадка */
const developer = new Programmer({
    name: 'John',
    age: 30,
    salary: 2000,
    lang: ['Ukrainian', 'English', 'Spanish'],
})

console.log(developer)

/* Створення об'єкту на основі класу-нащадка */
const manager = new Programmer({
    name: 'Antony',
    age: 25,
    salary: 1600,
    lang: ['Ukrainian', 'Portugal'],
})

console.log(manager)
